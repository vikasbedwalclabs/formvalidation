$(document).ready(function() {
    var arr2 = [];
    $("#success").hide();
    $("#header [href]").click(function() {
        $("#main").slideDown("slow", function() {
            $("#header p").html("All Field are Mandatory");
        });
    });
    //  code for hiding the errorMsg when starts enter any info in fields
    $("*").keydown(function(e)
    {
        $('#errorMsg').html("");
    });
    $(":checkbox").click(function()
    {
        $('#errorMsg').html("");
    });
    $("#contact_contactNo").keydown(function(e)
    {
//code to format phone no. to contain "-"
        if (e.keyCode !== 8) {
            if ($(this).val().length === 0) {
                $(this).val($(this).val() + "(");
            }
            else if ($(this).val().length === 4) {
                $(this).val($(this).val() + ")-");
            } else if ($(this).val().length === 9) {
                $(this).val($(this).val() + "-");
            }
        }
    });
//    Code for styling the box border when user focus on it
    $("input").focus(function() {
        $(this).css({"border": "2px solid #dadada", "border-radius": "5px", "outline": "none", "border-color": "#9ecaed", "box-shadow": "0 0 10px #9ecaed"});
    });
//    code for change the css effect on blur action    
    $("input").blur(function() {
        $(this).css({"border": "1px solid black", "border-radius": "5px", "outline": "none", "border-color": "black", "box-shadow": "0 0 0 black"});
    });
//    Regular expression for email validation
    var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
//    Regular expression for contact(US Based) validation
    var contactNo = /^[0-9-@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]*$/;
    $('#button').click(function() {
//        First Name Validation
        var arr = [];
        arr = document.getElementById("firstname").value;
        for (i = 0; i <= (arr.length); i++) {
            if (!isNaN(arr[i]) || (($('#firstname').val()) === "")) {
                $('#errorMsg').html("Enter valid First Name");
                document.getElementById("firstname").focus();
                return false;
                break;
            }
        }
//        Last name validation        
        var arr = [];
        arr = document.getElementById("lastname").value;
        for (i = 0; i <= (arr.length); i++) {
            if (!isNaN(arr[i]) || (($('#lastname').val()) === "")) {
                $('#errorMsg').html("Enter valid Last Name");
                document.getElementById("lastname").focus();
                return false;
                break;
            }
        }
//         Date of Birth validation(as like restricted to select future date)
        if ($('#contact_dob').val() === '') {
            document.getElementById("errorMsg").innerHTML = "Enter Valid DOB";
            document.getElementById("contact_dob").focus();
            return false;
        }
//          Adress field validation        
        else
        if ($('#contact_address').val() === '') {
            $('#errorMsg').html("Enter Address");
            document.getElementById("contact_address").focus();
            return false;
        }
//       Contact no(US Based as like 222-444-9090) validation    
        else
            var contactNo = /^[0-9-()-]*$/;
        if (!contactNo.test($("#contact_contactNo").val()) || $("#contact_contactNo").val() === "" || null) {
            $('#errorMsg').html("Enter valid Contact No");
            document.getElementById("contact_contactNo").focus();
            return false;
        }
//       Email validation    
        else
        if ($('#contact_email').val() === '' || !(emailFilter.test($('#contact_email').val()))) {
            $('#errorMsg').html("Enter Valid Email");
            document.getElementById("contact_email").focus();
            return false;
        }
//        validation for a strong password    
        else
        if ($('#contact_password').val() === '' || $('#contact_password').val().length < 6) {
            $('#errorMsg').html("Enter a Strong Password");
            document.getElementById("contact_password").focus();
            return false;
        }
//        Validation for confirm password field    
        else
        if ($('#contact_confirm_password').val() !== $('#contact_password').val()) {
            $('#errorMsg').html("Password Mismatch");
            document.getElementById("contact_confirm_password").focus();
            return false;
        }
//         validation for selection of atleast one hobbies    
        else
        if ($('input[type=checkbox]:checked').length === 0) {
            $('#errorMsg').html("Select atleast one Hobbie");
            document.getElementById("contact_hobbies").focus();
            return false;
        }
        $('#header').html('You have successfully submitted your information.');
        $("#main").slideUp(1000, function() {
            if (document.getElementById("contact_male").checked) {
                gender = "Male";
            }
            else {
                gender = "Female";
            }

            if (document.getElementById("checkBox1").checked && document.getElementById("checkBox2").checked) {
                hobbies = "PC Games, Net Surfing";
            }
            else
            if (document.getElementById("checkBox1").checked) {
                hobbies = "PC Games";
            }
            else {
                hobbies = "Net Surfing";
            }
            var arr2 = [$('#firstname').val(), $('#lastname').val(), gender, $('#contact_dob').val(), $('#contact_address').val(), $('#contact_contactNo').val(), $('#contact_email').val(), hobbies];
            $('#lblFname').html(arr2[0]);
            $('#lblLname').html(arr2[1]);
            $('#lblGender').html(arr2[2]);
            $('#lblDOB').html(arr2[3]);
            $('#lblAddress').html(arr2[4]);
            $('#lblContactNo').html(arr2[5]);
            $('#lblEmail').html(arr2[6]);
            $('#lblHobbies').html(arr2[7]);
            $('#success').show();
        });
    });
    $('#editButton').click(function() {
        $("#header").html("All Field are Mandatory.");
        $("#success").hide();
        $("#main").slideDown();
    });
    /*Date Constraint*/
    var dt = new Date();
    var yr = dt.getFullYear();
    var mn = (dt.getMonth()) + 1;
    if (mn.toString().length === 1) {
        mn = "0" + mn;
    }
    else {
        return mn;
    }
    var dy = dt.getDate();
    var maxvl = (yr + "-" + mn + "-" + dy);
    $("#contact_dob").attr("max", maxvl);
    $("#contact_dob").attr("value", maxvl);
});


    